import java.util.Arrays;


public class Element {

	private int numberOfElement;
	private int NOP1;
	private int NOP2;
	private double k;
	private double l;
	private double detJ;
	private double cp;
	private double rho;
	
	public double[][] localMatrix;
	public double[] localVector;
	
	public Element(int id, int NOP1, int NOP2, double k, double cp, double rho){
		this.numberOfElement = id;
		this.NOP1 = NOP1;
		this.NOP2 = NOP2;
		this.k = k;
		this.l = 0;
		this.cp = cp;
		this.rho = rho;
		this.localMatrix = new double[2][2];
		this.localVector = new double[2];
		
		for(int i=0; i<2; i++){
			localMatrix[0][i] = 0;
			localMatrix[1][i] = 0;
			localVector[i] = 0;
		}		
	}
	
	public void setL(double nop1, double nop2){
		this.l = nop2 - nop1;
		this.l = 0.01;
		this.detJ = this.l/2;
	}

	public int getNumberOfElement() {
		return numberOfElement;
	}

	public int getNOP1() {
		return NOP1;
	}

	public int getNOP2() {
		return NOP2;
	}

	public double getK() {
		return k;
	}

	public double getL() {
		return l;
	}
	
	public double getShapeFunction(int numberOfFunction, double point){
		switch(numberOfFunction){
		case 1:{
			return 0.5*(1-point);
		}
		case 2:{
			return 0.5*(1+point);
		}
		default: return 0;
		}
	}

	public void calculateLocalMatrixAndVector(GlobalData gd, Mesh mainMesh){
		IntegrationSchema is = gd.IS;
		for(int i=0; i<is.getNumberOfPoints();i++){
			double rp = getRp(is.pointsOfIntegration[i], getNOP1(), getNOP2(),mainMesh);
			this.localMatrix[0][0] += this.k/this.l*rp*is.wages[i]
					+this.getCp()*this.getRho()*this.l/gd.deltaTau*
					getShapeFunction(1, is.pointsOfIntegration[i])*
					getShapeFunction(1, is.pointsOfIntegration[i])*
					rp*is.wages[i];
			
			this.localMatrix[0][1] += -1*this.k/this.l*rp*is.wages[i]
					+this.getCp()*this.getRho()*this.l/gd.deltaTau*
					getShapeFunction(1, is.pointsOfIntegration[i])*
					getShapeFunction(2, is.pointsOfIntegration[i])*
					rp*is.wages[i];
					
			this.localMatrix[1][0] = this.localMatrix[0][1];
			
			this.localMatrix[1][1] += this.k/this.l*rp*is.wages[i]
					+this.getCp()*this.getRho()*this.l/gd.deltaTau*
					getShapeFunction(2, is.pointsOfIntegration[i])*
					getShapeFunction(2, is.pointsOfIntegration[i])*
					rp*is.wages[i];
			
			calculateLocalVector(gd, is, i, rp, mainMesh);
		}
	}
	
	public double getRp(double point, int nop1, int nop2, Mesh mainMesh){
		return getShapeFunction(1, point)*mainMesh.nodes[nop1].getPosition()+
				getShapeFunction(2, point)*mainMesh.nodes[nop2].getPosition();
	}
	
	public void calculateLocalVector(GlobalData gd, IntegrationSchema is, int i, double rp, Mesh mainMesh){
		this.localVector[0] += -1*this.getCp()*this.getRho()*this.l/gd.deltaTau*
				(getShapeFunction(1, is.pointsOfIntegration[i])*mainMesh.nodes[NOP1].getTemperature()+
				getShapeFunction(2, is.pointsOfIntegration[i])*mainMesh.nodes[NOP2].getTemperature())*
				getShapeFunction(1, is.pointsOfIntegration[i])*rp*is.wages[i];
		
		this.localVector[1] += -1*this.getCp()*this.getRho()*this.l/gd.deltaTau*
				(getShapeFunction(1, is.pointsOfIntegration[i])*mainMesh.nodes[NOP1].getTemperature()+
				getShapeFunction(2, is.pointsOfIntegration[i])*mainMesh.nodes[NOP2].getTemperature())*
				getShapeFunction(2, is.pointsOfIntegration[i])*rp*is.wages[i];
	}
	
	public void addBoundaryConditions(Mesh mainMesh, int nop){
		
		if(nop == 1){
			localMatrix[0][0] +=
					2*mainMesh.globalData.getAplha()*mainMesh.nodes[this.NOP1].getPosition();
			
			localVector[0] -=
					2*mainMesh.globalData.getAplha()*
					mainMesh.nodes[this.NOP1].getPosition()*
					mainMesh.globalData.getAmbientTemperature();
		}
		
		else if (nop == 2){
			localMatrix[1][1] +=
					2*mainMesh.globalData.getAplha()*mainMesh.nodes[this.NOP2].getPosition();
			
			localVector[1] -=
					2*mainMesh.globalData.getAplha()*
					mainMesh.nodes[this.NOP2].getPosition()*
					mainMesh.globalData.getAmbientTemperature();
		}
		
	}
	
	@Override
	public String toString() {
		return "Element [numberOfElement=" + numberOfElement + ", NOP1=" + NOP1
				+ ", NOP2=" + NOP2 + ", k=" + k + ", l=" + l
				+ ", cp=" + cp + ",rho=" + rho
				+ ", localMatrix=" + Arrays.toString(localMatrix[0])+
									 Arrays.toString(localMatrix[1])
				+ ", localVector=" + Arrays.toString(localVector) + "]";
	}

	public double getDetJ() {
		return detJ;
	}

	public void setDetJ(double detJ) {
		this.detJ = detJ;
	}

	public double getCp() {
		return cp;
	}

	public void setCp(double cp) {
		this.cp = cp;
	}

	public double getRho() {
		return rho;
	}

	public void setRho(double rho) {
		this.rho = rho;
	}

}
