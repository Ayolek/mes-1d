import java.util.Arrays;


public class Mesh {

	public double[][] globalMatrix;
	public double[] globalVector;
	public Element[] elements;
	public Node[] nodes;
	public GlobalData globalData;
	public double[][] systemOfEquations;
	public double[] temperatures;
	
	public Mesh(int numberOfNodes, GlobalData GD){
		this.globalMatrix = new double[numberOfNodes][numberOfNodes];
		this.globalVector = new double[numberOfNodes];
		this.elements = new Element[numberOfNodes-1];
		this.nodes = new Node[numberOfNodes];
		this.globalData = GD;
		this.systemOfEquations = new double[numberOfNodes][numberOfNodes+1];
		this.temperatures = new double[numberOfNodes];
		
		for(int i=0; i<numberOfNodes; i++){
			for(int j=0;j<numberOfNodes;j++){
				globalMatrix[i][j] = 0;
			}
			globalVector[i]=0;
		}
	}
	
	public void generateGlobalMatrixAndVector(){
		
		for(int i=0; i<globalData.getNumberOfElements(); i++){
			int n1=this.elements[i].getNOP1();
			int n2=this.elements[i].getNOP2();
			globalMatrix[n1][n1] += elements[i].localMatrix[0][0];
			globalMatrix[n1][n2] += elements[i].localMatrix[0][1];		
			globalMatrix[n2][n1] += elements[i].localMatrix[1][0];		
			globalMatrix[n2][n2] += elements[i].localMatrix[1][1];		
			
			globalVector[n1] += elements[i].localVector[0];
			globalVector[n2] += elements[i].localVector[1];
			
		}
	}
	
	public void setSystemOfEquations(){
		for(int i=0;i<globalData.getNumberOfNodes()+1;i++)
			for(int j=0;j<globalData.getNumberOfNodes();j++){
				if(i!=globalData.getNumberOfNodes())
					systemOfEquations[j][i] = globalMatrix[j][i];
				else{
					systemOfEquations[j][i] = globalVector[j];
				}
			}
	}
	
	public int solveSystemOfEquation(){
		int max;
		double tmp;
		int N = globalData.getNumberOfNodes();
		double[][] a=systemOfEquations;
		
		for(int i=0; i<N; i++){ // eliminacja
			max=i;
			for(int j=i+1; j<N; j++)
				if(Math.abs(a[j][i])>Math.abs(a[max][i])) max=j; //fabs(x)=|x|, warto�� bezwzgl�dna dla danych double 
			for(int k=i; k<N+1; k++){ // zamiana wierszy warto�ciami
				tmp=a[i][k]; a[i][k]=a[max][k];
				a[max][k]=tmp;
			}
			if(a[i][i]==0)
				return 0;  // Uk�ad sprzeczny!
			for(int j=i+1; j<N; j++)
				for(int k=N; k>=i; k--) // mno�enie wiersza j przez wsp�czynnik "zeruj�cy":
					a[j][k]=a[j][k]-a[i][k]*a[j][i]/a[i][i];
			} // redukcja wsteczna
		
		for(int j=N-1; j>=0; j--){
			tmp=0;
			for(int k=j+1; k<N; k++)
				tmp=tmp+a[j][k]*temperatures[k];
			temperatures[j]=(a[j][N]-tmp)/a[j][j];
			}
		for(int i=0; i<N; i++){
			temperatures[i]*=-1;
		}
		return 1;  // wszystko w porz�dku! 
	}

	@Override
	public String toString() {
		return "Mesh [globalMatrix=" + showMatrix(globalData.getNumberOfNodes())+"]\n"
				+ "Mesh [globalVector=" + Arrays.toString(globalVector)+"]\n"
				+ "Mesh [systemOfEquations=" + showEquation(globalData.getNumberOfNodes())+"]";
	}
	
	public void clearData(){
		for(int j=0;j<globalData.getNumberOfElements();j++){
			for(int i=0; i<2; i++){
				this.elements[j].localMatrix[0][i] = 0;
				this.elements[j].localMatrix[1][i] = 0;
				this.elements[j].localVector[i] = 0;
			}
		}
		for(int i=0;i<this.globalData.getNumberOfNodes();i++){
			for(int j=0;j<this.globalData.getNumberOfNodes();j++){
				globalMatrix[i][j] = 0;
			}
			globalVector[i] = 0;
			//this.nodes[i].setTemperature(temperatures[i]);
			double averageTemperature = interpolateTemperature();
			this.nodes[i].setTemperature(averageTemperature);
		}
	}
	
	public double interpolateTemperature(){
		double sum = 0.0;
		int i=0;
		for(;i<temperatures.length;i++){
			sum+=temperatures[i];
		}
		return sum/i;
	}
	
	public String showMatrix(int count){
		String output="";
		for(int i=0; i<count; i++){
			output += Arrays.toString(globalMatrix[i]);
		}
		
		return output;
	}
	
	public String showEquation(int count){
		String output="";
		for(int i=0; i<count; i++){
			output += Arrays.toString(systemOfEquations[i]);
		}
		
		return output;
	}
}
