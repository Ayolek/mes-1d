import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		
			File file = new File(args[0]);
			//File file = new File("input.txt");
			
			Scanner in = new Scanner(file);
			
			GlobalData globalData = new GlobalData(in.nextInt(),
					in.nextDouble(), in.nextDouble(),
					in.nextDouble());
			
			Mesh mainMesh = new Mesh(globalData.getNumberOfNodes(), globalData);
			
			for(int i=0; i<globalData.getNumberOfNodes(); i++){
				int id = in.nextInt();
				mainMesh.nodes[id] = new Node(id, in.nextDouble(), in.nextInt(), globalData.getInitialTemperature());
			}
			
			for(int i=0; i<globalData.getNumberOfElements(); i++){
				int id = in.nextInt();
				mainMesh.elements[id] = new Element(id, in.nextInt(),
						in.nextInt(), in.nextDouble(),in.nextDouble(),in.nextDouble());
				mainMesh.elements[id].setL(mainMesh.nodes[mainMesh.elements[id].getNOP1()].getPosition(),
											mainMesh.nodes[mainMesh.elements[id].getNOP2()].getPosition());
			}
			
			in.close();
			
			PrintWriter writer = new PrintWriter("temperatures.txt", "UTF-8");
			
			for(int step = globalData.deltaTau;step<=10000;step+=globalData.deltaTau){
			
				for(int i=0; i<globalData.getNumberOfElements(); i++){
					mainMesh.elements[i].calculateLocalMatrixAndVector(globalData, mainMesh);
						if(mainMesh.nodes[mainMesh.elements[i].getNOP1()].getBc1()!=0){
							mainMesh.elements[i].addBoundaryConditions(mainMesh, 1);
						}else if(mainMesh.nodes[mainMesh.elements[i].getNOP2()].getBc1()!=0){
							mainMesh.elements[i].addBoundaryConditions(mainMesh, 2);
						}
					
				}
				
				mainMesh.generateGlobalMatrixAndVector();
				mainMesh.setSystemOfEquations();
				mainMesh.solveSystemOfEquation();
				//showData(mainMesh);
				writer.print(step/60);
				for(int i=0; i<mainMesh.temperatures.length; i++){				
					writer.print("\t"+mainMesh.temperatures[i]);
				}
				writer.println();
				
				mainMesh.clearData();
			}
			writer.close();
	}
	
	public static void showData(Mesh mainMesh){
		DecimalFormat df = new DecimalFormat("#.##");
		System.out.println(mainMesh.globalData.toString());
		for(int i=0 ; i<mainMesh.globalData.getNumberOfNodes() ; i++){
			System.out.println(mainMesh.nodes[i].toString());
		}
		for(int i=0 ; i<mainMesh.globalData.getNumberOfElements() ; i++){
			System.out.println(mainMesh.elements[i].toString());
		}
		
		System.out.println(mainMesh.toString());
		
		int success = mainMesh.solveSystemOfEquation();
				
		if(success == 1){
			for(int i=0; i<mainMesh.globalData.getNumberOfNodes(); i++){
				System.out.print("t"+i+" = "+df.format(mainMesh.temperatures[i])+"\n");
			}
		}
	}
}

