
public class GlobalData {
	private int numberOfElements;
	private int numebrOfNodes;
	private double aplha;
	private double q;
	private double initialTemperature;
	private double ambientTemperature;
	public IntegrationSchema IS;
	public int deltaTau;
	
	public GlobalData(int numberOfElements, double alpha,
					  double initialTemperature,
					  double ambientTemperature){
		this.numberOfElements = numberOfElements;
		this.numebrOfNodes = numberOfElements+1;
		this.aplha = alpha;
		this.initialTemperature = initialTemperature;
		this.IS = new IntegrationSchema(2);
		this.setAmbientTemperature(ambientTemperature);
		this.deltaTau = 60;
	}

	public int getNumberOfElements() {
		return numberOfElements;
	}

	public int getNumberOfNodes() {
		return numebrOfNodes;
	}

	public double getAplha() {
		return aplha;
	}

	public double getQ() {
		return q;
	}

	public double getInitialTemperature() {
		return initialTemperature;
	}

	public void setInitialTemperature(double initialTemperature) {
		this.initialTemperature = initialTemperature;
	}

	public double getAmbientTemperature() {
		return ambientTemperature;
	}

	public void setAmbientTemperature(double ambientTemperature) {
		this.ambientTemperature = ambientTemperature;
	}

	@Override
	public String toString() {
		return "GlobalData [numberOfElements=" + numberOfElements
				+ ", numebrOfNodes=" + numebrOfNodes + ", aplha=" + aplha
				+ ", initialTemperature=" + initialTemperature
				+ ", ambientTemperature=" + ambientTemperature + ", Integration Schema=" + IS.getNumberOfPoints()
				+ ", deltaTau=" + deltaTau + "]";
	}

}
