
public class IntegrationSchema {
	private int numberOfPoints;
	public double[] pointsOfIntegration;
	public double[] wages;
	
	public IntegrationSchema(int numberOfPoints){
		switch(numberOfPoints){
		case 2:{
			this.setNumberOfPoints(2);
			this.pointsOfIntegration = new double[2];
			this.pointsOfIntegration[0] = -0.577;
			this.pointsOfIntegration[1] = 0.577;
			this.wages = new double[2];
			this.wages[0] = 1;
			this.wages[1] = 1;
			break;
		}
		}
	}

	public int getNumberOfPoints() {
		return numberOfPoints;
	}

	public void setNumberOfPoints(int numberOfPoints) {
		this.numberOfPoints = numberOfPoints;
	}
}
