
public class Node {

	private int numberOfNode;
	private double position;
	private int bc1;
	private double temperature;
	
	public Node(int id, double position, int bc1, double temperature){
		this.numberOfNode = id;
		this.position = position;
		this.bc1 = bc1;

		this.setTemperature(temperature);
	}

	public int getNumberOfNode() {
		return numberOfNode;
	}

	public double getPosition() {
		return position;
	}

	public int getBc1() {
		return this.bc1;
	}
	
	public String showBc() {
		String Final = "NONE";
		switch(this.bc1){
		case BoundaryCondition.EXCHANGE: Final = "EXCHANGE";break;
		case BoundaryCondition.FLUX: Final = "FLUX";break;
		case BoundaryCondition.NONE: Final = "NONE";break;
		} 
		return Final;
	}

	@Override
	public String toString() {
		return "Node [numberOfNode=" + numberOfNode + ", position=" + position
				+ ", BoundaryCondition=" + showBc() + "]";
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

}
